require 'sidekiq/web'

Rails.application.routes.draw do
  mount Sidekiq::Web => '/sidekiq'
  namespace :api do
    resources :event_logs, path: 'eventlogs', only: [:create]
  end

  resources :question1, only: [] do
    post :answer, on: :collection
  end
  resources :question2, only: [] do
    post :answer, on: :collection
  end
  resources :question3, only: [] do
    post :answer, on: :collection
  end
  resources :question4, only: [] do
    post :answer, on: :collection
  end
  resources :question5, only: [] do
    post :answer, on: :collection
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

require 'rails_helper'

RSpec.describe FileLoggerJob, type: :job do
    let(:message) { "Log message" }
    let(:file_name) { "log/skylablog.#{Rails.env}.txt" }

    before do
      Timecop.freeze
    end

    after do
      Timecop.return
    end

    it "should append new content" do
      expect(File).to receive(:open).with(file_name, 'a').and_return(File.open(file_name, 'a'))
      described_class.perform_now(message, file_name, 1)
    end

    it "should break file to small files" do
      described_class.perform_now(message, file_name, 0.001)
      expect(File.exist?("log/skylablog.#{Rails.env}.txt.#{DateTime.now.strftime('%F_%T')}")).to be true
    end
end

require 'rails_helper'

RSpec.describe EventLogJob, type: :job do
  let(:event_name) { "EventName" }
  let(:timestamp) { "2017-10-01 06:00:01" }

  before do
    ActiveJob::Base.queue_adapter = :test
  end

  it "matches with enqueued job" do
    expect {
      EventLogJob.perform_later(event_name, timestamp)
    }.to have_enqueued_job.with(event_name, timestamp).on_queue("default")
  end

  it "create a new EventJob" do
    expect {
      EventLogJob.perform_now(event_name, timestamp)
    }.to change { EventLog.count }.by(1)
  end
end

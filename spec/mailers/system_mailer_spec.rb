require "rails_helper"

RSpec.describe SystemMailer, type: :mailer do
	let(:message) { "log message" }
  let(:mail) { described_class.log(message).deliver_now }

	it 'renders the subject' do
		expect(mail.subject).to eq('SystemLog')
	end

	it 'renders the receiver email' do
		expect(mail.to).to eq([ENV['devop_email']])
	end

end

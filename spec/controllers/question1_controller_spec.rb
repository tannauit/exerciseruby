require 'rails_helper'

RSpec.describe Question1Controller, type: :controller do

  describe "POST #answer" do
    context "pass array to parameters" do
      let(:input) { [9,1,8,2,7,3,6,4,5,10,13] }
      it "returns http success" do
        post :answer, params: { input: input }
        expect(response).to have_http_status(:success)
      end

      it "should return the output with sorted" do
        post :answer, params: { input: input }
        expect(JSON.parse(response.body)).to eq('lowest_highest' => [1,2,3,4,5,6,7,8,9,10,13], 'highest_lowest' => [13,10,9,8,7,6,5,4,3,2,1])
      end
    end

    context "invalid parameters" do
      it "should return error" do
        post :answer
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

end

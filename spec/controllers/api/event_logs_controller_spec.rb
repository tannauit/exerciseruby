require 'rails_helper'

RSpec.describe Api::EventLogsController, type: :controller do

  describe "POST #create" do
    context "valid parrameters" do
      let(:event_name) { "EventName" }
      let(:timestamp) { "2017-10-01 06:00:01" }

      it "returns http success" do
        post :create, params: { event_name: event_name, timestamp: timestamp }
        expect(response).to have_http_status(:success)
      end
      it "create a job" do
        ActiveJob::Base.queue_adapter = :test
        expect {
          post :create, params: { event_name: event_name, timestamp: timestamp }
        }.to have_enqueued_job(EventLogJob).with(event_name, timestamp)
      end
    end

    context "invalid parameters" do
      it "should return error" do
        post :create
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

end

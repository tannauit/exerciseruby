require 'rails_helper'

RSpec.describe Question5Controller, type: :controller do

  describe "POST #answer" do
    context "valid parameters" do
      let(:message) { "log message test" }
      it "returns http success" do
        post :answer, params: { message: message }
        expect(response).to have_http_status(:success)
      end

      it "call email" do
        expect_any_instance_of(SkyLabLogger).to receive(:email)
        post :answer, params: { message: message }
      end

      it "call console" do
        expect_any_instance_of(SkyLabLogger).to receive(:console)
        post :answer, params: { message: message }
      end

      it "call log_to_file" do
        expect_any_instance_of(SkyLabLogger).to receive(:log_to_file)
        post :answer, params: { message: message }
      end
    end

    context "invalid parameters" do
      it "should return error" do
        post :answer
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

end

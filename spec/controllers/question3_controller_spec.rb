require 'rails_helper'

RSpec.describe Question3Controller, type: :controller do

  describe "POST #answer" do
    context "post string param" do
      let(:string_value) { "abcdabcdabcdabcdeabcdcbaf" }

      it "returns http success" do
        get :answer, params: { string_value: string_value }
        expect(response).to have_http_status(:success)
      end

      it "return first non-repeated" do
        get :answer, params: { string_value: string_value }
        expect(response.body).to eq("e")
      end

      it "return nil when string is empty" do
        get :answer, params: { string_value: "" }
        expect(response.body).to eq("null")
      end

      it "return nil when all characters are repeated" do
        get :answer, params: { string_value: "abccbadada" }
        expect(response.body).to eq("null")
      end
    end

    context "missing parram" do
      it "should return error" do
        post :answer
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

end

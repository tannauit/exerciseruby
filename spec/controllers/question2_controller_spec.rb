require 'rails_helper'

RSpec.describe Question2Controller, type: :controller do

  describe "POST #answer" do
    context "pass array to parameters" do
      let(:array1) { [3, 4, 6, 3, 4, 7, 8, 5, 9, 3] }
      let(:array2) { [5, 4, 1, 2, 3, 4, 3] }
      it "returns http success" do
        post :answer, params: { array1: array1, array2: array2 }
        expect(response).to have_http_status(:success)
      end

      it "should return the output with sorted" do
        post :answer, params: { array1: array1, array2: array2 }
        expect(JSON.parse(response.body)).to eq(["3", "4", "5"])
      end
    end

    context "invalid parameters" do
      it "should return error" do
        post :answer
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

end

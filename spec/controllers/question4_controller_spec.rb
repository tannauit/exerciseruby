require 'rails_helper'

RSpec.describe Question4Controller, type: :controller do

  describe "GET #answer" do
    context "post string param" do
      let(:string_value) { "zyabcdabcac" }

      it "returns http success" do
        get :answer, params: { string_value: string_value }
        expect(response).to have_http_status(:success)
      end

      it "return the json of data" do
        get :answer, params: { string_value: string_value }
        expect(JSON.parse(response.body)).to eq({
          "highest_lowest" => [{"c" => 3}, {"a" => 3}, {"b" => 2}, {"d" => 1}, {"y" => 1}, {"z" => 1}],
          "lowest_highest" => [{"z" => 1}, {"y" => 1}, {"d" => 1}, {"b" => 2}, {"a" => 3}, {"c" => 3}]
        })
      end
    end

    context "missing parram" do
      it "should return error" do
        post :answer
        expect(response).to have_http_status(:unprocessable_entity)
      end
    end
  end

end

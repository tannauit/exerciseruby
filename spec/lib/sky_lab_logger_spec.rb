require 'rails_helper'

RSpec.describe SkyLabLogger do
  let(:message) { "Log message" }
  subject { described_class.new(message) }

  describe "log_to_file" do
    before do
			ActiveJob::Base.queue_adapter = :test
    end

    it "should enqueued job" do
      file_name = "log/skylablog.#{Rails.env}.txt"
      expect{ subject.log_to_file }.to have_enqueued_job(FileLoggerJob)
    end
  end

  describe "console" do
    it "should print debug message" do
      expect(STDOUT).to receive(:puts)
      subject.console
    end
  end

  describe "email" do
		before do
			ActiveJob::Base.queue_adapter = :test
		end

    it "should queued an email" do
      expect { subject.email }
      .to have_enqueued_job
    end
  end
end

require 'rails_helper'

RSpec.describe EventLog, type: :model do
  it { should validate_presence_of :event_name }
  it { should validate_presence_of :timestamp }
end

# README

## Install 

1. install rvm by following the instruction in https://rvm.io/
2. cd to project directory, it will switch to ruby-version and gemset, please make sure you have the same version
3. run `gem install bundler`
4. run `bundle install`
5. copy `config/database.yml.example` to `config/database.yml`
6. copy `config/application.yml.example` to `config/application.yml`


## Run app
1. Start `redis` in your lodal
2. Run `rails s`

## Test api

1. Question1
`curl --request POST --url http://localhost:3000/question1/answer --header 'content-type: application/json' --data '{"input":[9,1,8,2]}'`

2. Question2
`curl --request POST --url http://localhost:3000/question2/answer --header 'content-type: application/json' --data '{"array1":[9,1,8,2,3,7,9],"array2":[2,3,4,7,5,9,9]}'`

3. Question3
`curl --request POST --url http://localhost:3000/question3/answer --header 'content-type: application/json' --data '{"string_value":"abccbaaaddafaeadgadah"}'`
4. Question4
`curl --request POST --url http://localhost:3000/question4/answer --header 'content-type: application/json' --data '{"string_value":"abccbaaaddafaeadgadah"}'`
5. EventLog
`curl --request POST --url http://localhost:3000/api/eventlogs --header 'content-type: application/json' --data '{"event_name": "test","timestamp":"2017-10-01 06:00:01"}'`
6. Question5
`curl --request POST --url http://localhost:3000/question5/answer --header 'content-type: application/json' --data '{"message": "log message"}'`


class SystemMailer < ApplicationMailer
  default to: ENV['devops_email']

	def log(message)
		@message = message
		mail(subject: 'SystemLog')
	end
end

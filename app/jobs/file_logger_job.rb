class FileLoggerJob < ApplicationJob
  queue_as :default

  def perform(message, file_name, max_size)
    f = File.open(file_name, 'a')
    f.write(message)
    f.close
    if File.size(file_name) >= max_size
      File.rename(file_name, "#{file_name}.#{DateTime.now.strftime('%F_%T')}")
    end
  end
end

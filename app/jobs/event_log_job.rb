class EventLogJob < ApplicationJob
  queue_as :default

  def perform(event_name, timestamp)
    EventLog.create!(event_name: event_name, timestamp: timestamp)
  end
end

class Question3
  def self.first_non_repeated(characters)
    repeated_items = {}

    characters.each do |char|
      if repeated_items[char].nil?
        repeated_items[char] = false
      else
        repeated_items[char] = true
      end
    end
    characters.each do |char|
      return char if repeated_items[char] == false
    end
    return nil
  end
end

class Question1
  def self.bubble_sort(array)
    count = array.length
    return if count < 2

    loop do
      swapped = false
      for i in 1...count
        if array[i - 1] > array[i]
          temp = array[i - 1]
          array[i - 1] = array[i]
          array[i] = temp
          swapped = true
        end
      end
      break unless swapped
    end
  end
end

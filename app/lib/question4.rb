class Question4

  def self.get_sorted_repeated_count(characters)
    repeated_counted_array = count_repeated(characters)
    bubble_sort(repeated_counted_array)
    repeated_counted_array
  end

  private
  # result [[char, count], [char, count]]
  def self.count_repeated(characters)
    repeated_items = {}
    characters.each do |char|
      repeated_items[char] ||= 0
      repeated_items[char] += 1
    end
    repeated_items.map{|key, value| [key, value]}
  end

  # params [[key, value], [key, value]]
  def self.bubble_sort(array)
    count = array.length
    return if count < 2

    loop do
      swapped = false
      for i in 1...count
        if array[i - 1][1] > array[i][1]
          temp = array[i - 1]
          array[i - 1] = array[i]
          array[i] = temp
          swapped = true
        end
      end
      break unless swapped
    end
  end
end

class Question2
  def self.common_elements(array1, array2)
    count1 = array1.length
    count2 = array2.length
    return [] if count1 == 0 || count2 == 0

    Question1.bubble_sort(array1)
    Question1.bubble_sort(array2)
    common = []
    i = j = 0

    while i < count1 && j < count2
      if array1[i] == array2[j]
        if common.length == 0 || common.last != array1[i]
          common.push(array1[i])
        end
        i += 1
        j += 1
      elsif array1[i] < array2[j]
        i += 1
      else
        j += 1
      end
    end
    return common
  end
end

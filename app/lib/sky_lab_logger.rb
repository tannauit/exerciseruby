class SkyLabLogger
  MAX_FILE_SIZE = 100

  def initialize(message)
    @message = "#{DateTime.now.strftime('%F %T')}: #{message}\n"
  end

	def log_to_file
		file_name = "log/skylablog.#{Rails.env}.txt"
		FileLoggerJob.perform_later(@message, file_name, MAX_FILE_SIZE)
	end

  def console
    puts @message
  end

  def email
    SystemMailer.log(@message).deliver_later
  end
end

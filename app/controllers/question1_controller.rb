class Question1Controller < ApplicationController
  def answer
    return render status: :unprocessable_entity, json: { message: 'missing input param' } unless input = params[:input]

    input = input.map(&:to_i)
      Question1.bubble_sort(input)
    lowest_highest = input
    highest_lowest = lowest_highest.reverse

    render status: :ok, json: { lowest_highest: lowest_highest, highest_lowest: highest_lowest}
  end
end

class Question5Controller < ApplicationController
  def answer
    return render status: :unprocessable_entity, json: { message: 'missing message param' } unless message = params[:message]

    logger = SkyLabLogger.new(message)
    logger.console
    logger.email
    logger.log_to_file

    render status: :ok, json: { message: "Logged message" }
  end
end

class Question3Controller < ApplicationController
  def answer
    return render status: :unprocessable_entity, json: { message: 'missing params' } unless string_value = params[:string_value]

    characters = string_value.scan /\w/
    result = Question3.first_non_repeated(characters)

    render status: :ok, json: result
  end
end

class Question2Controller < ApplicationController
  def answer
    array1 = params[:array1]
    array2 = params[:array2]
    return render status: :unprocessable_entity, json: { message: 'missing params' } unless array1 || array2

    result = Question2.common_elements(array1, array2)
    render status: :ok, json: result
  end
end

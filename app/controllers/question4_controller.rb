class Question4Controller < ApplicationController
  def answer
    return render status: :unprocessable_entity, json: { message: 'missing params' } unless string_value = params[:string_value]

    characters = string_value.scan /\w/
    lowest_highest = Question4.get_sorted_repeated_count(characters)
    highest_lowest = lowest_highest.reverse

    render status: :ok, json: { lowest_highest: lowest_highest.map{ |key, value| {key => value} }, highest_lowest: highest_lowest.map{|key, value| {key => value} }}
  end
end

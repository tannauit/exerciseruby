class Api::EventLogsController < ApplicationController
  def create
    event_name = params[:event_name]
    timestamp = params[:timestamp]
    return render status: :unprocessable_entity, json: { message: 'missing params' } unless event_name || timestamp

    EventLogJob.perform_later(event_name, timestamp)

    render status: :ok, json: { message: 'EventLog was queued' }
  end
end
